<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __costruct()
    {
        $this->middleware('auth:api', ['except'=>['login','register']]);
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($Validator->fails()) {
            $pesan = $validator->errors();

            return $this->failedResponse($pesan,422);
        }

        $credentials = request(['username', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return $this->FailedResponse('Username atau password salah!',401);
        }

    }
    private function success($data,$statusCode,$message='success')
    {
        return response()->json([
            'status'=>false,
            'message'=>$message,
            'data'=>null,
            'status_code'=>$statusCode
        ],$statusCode);
    }

    private function failedResponse($message,$statusCode)
    {
        return response()->json([
            'status'=>false,
            'message'=>$message,
            'data'=>null,
            'status_code'=>$statusCode
        ],$statusCode);
    }
}
